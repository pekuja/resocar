## Resocar

This is the source code for Resocar, the bot that came in 3rd place in the first ever Coding World Championship, Hello World Open 2014.

You can find a Visual Studio compatible (we used MonoDevelop/Xamarin Studio) project file in mono/Resocar. The source code is in mono/Resocar/bot.cs (yes, all of it).